package com.app.controller;

import com.app.dto.NoteDTO;
import com.app.model.MyJson;
import com.app.model.User;
import com.app.model.Users;
import com.app.service.NoteService;

import java.util.Collection;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.security.oauth2.client.OAuth2RestTemplate;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;


@Controller
public class HomeController {

    @Autowired
    NoteService noteService;

    @Autowired
    OAuth2RestTemplate restTemplate;
    
    @Value("${resource-server}/APIRest")
    private String restResourceContrl;

    
    @RequestMapping("/")
    public String home() {
        return "redirect:/notes";
    }

    @RequestMapping("/notes")
    public String notes(Model model) {
        model.addAttribute("notes", noteService.getAllNotes());
        return "index";
    }

    @RequestMapping("/add")
    public String add(Model model) {
        model.addAttribute("note", new NoteDTO());
        return "add";
    }
    @RequestMapping(value ="/getUsers" ,method=RequestMethod.GET)
    public Object[] add() {
    	
        return noteService.getAllUsers();
    }


    @RequestMapping(value = "/add", method = RequestMethod.POST)
    public String add(NoteDTO noteDTO, Model model) { 
        NoteDTO savedNote = noteService.addNote(noteDTO);

        if(savedNote != null){
            return "redirect:/notes";
        }else{
            model.addAttribute("note", noteDTO);
            return "add?error";
        }

    }
    
    @RequestMapping(value = "/addNewUser", method = RequestMethod.POST)
    public ResponseEntity<Users> addUsers(@RequestBody Users user){
    	
        ResponseEntity<Users> response = restTemplate.postForEntity(restResourceContrl+"/addNew", user, Users.class);
	       
		return response;
    	
    }
    
   
}
