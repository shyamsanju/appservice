package com.app.service.impl;

import com.app.dto.NoteDTO;
import com.app.model.MyJson;
import com.app.model.User;
import com.app.model.Users;
import com.app.service.NoteService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.hateoas.Resources;
import org.springframework.http.ResponseEntity;
import org.springframework.security.oauth2.client.OAuth2RestTemplate;
import org.springframework.stereotype.Service;

import java.util.Collection;
import java.util.List;


@Service
public class NoteServiceImpl implements NoteService {
	
    @Autowired
    OAuth2RestTemplate restTemplate;

    @Value("${resource-server}/note")
    private String notesURL;



    @Value("${resource-server}/APIRest")
    private String restResourceContrl;

    @Override
    public Collection<NoteDTO> getAllNotes() {
        Resources<NoteDTO> notes = restTemplate.getForObject(notesURL, Resources.class);
        return notes.getContent();
    }

    @Override
    public NoteDTO addNote(NoteDTO note) {
        ResponseEntity<NoteDTO> response = restTemplate.postForEntity(notesURL, note, NoteDTO.class);
        return response.getBody();
    }

	@Override
	public  Object[] getAllUsers() {
		 Object[] notes =  restTemplate.getForObject(restResourceContrl+"/getAllUsers", Object[].class);
	        return   notes;
	}
	
	 
}
