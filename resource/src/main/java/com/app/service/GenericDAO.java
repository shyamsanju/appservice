package com.app.service;

import java.util.List;

import org.springframework.stereotype.Component;

import com.app.model.MyJson;

@Component
public interface GenericDAO<T> {


List<T> getAllData(String fileName);

	
}
