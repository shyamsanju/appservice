package com.app.service;

import java.util.List; 

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class GenericServiceLayer<T> implements GenericService<T>{

	@Autowired
	GenericDAO<T> genericDAO;
	
	public  List<T> getAllUsers() {
		List<T> usersList = (List<T>) genericDAO.getAllData("users.json");
		return usersList;
	}

	@Override
	public List<T> getAllOrgData() {
		List<T> dataList = (List<T>) genericDAO.getAllData("metadata.json");
		return dataList;
	}
}
