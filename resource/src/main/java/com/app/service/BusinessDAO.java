package com.app.service;

import java.util.List;

import org.springframework.stereotype.Component;

import com.app.model.Bussiness;

@Component
public interface BusinessDAO {

	

	Bussiness findById(int id);     
  
    void save(Bussiness bussiness);
     
    void deleteById(int id);
     
    List<Bussiness> findAllBussiness();
}

