package com.app.service;

import java.util.List;

import org.springframework.stereotype.Component;

import com.app.model.MyJson;
import com.app.model.OrgModel;
import com.app.model.User;
@Component
public interface GenericService<T> {

	public List<T> getAllOrgData();

	public List<T> getAllUsers();
	
}
