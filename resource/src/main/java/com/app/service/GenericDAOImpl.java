package com.app.service;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.app.model.MyJson;
import com.app.model.OrgModel;
import com.app.model.User;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;

@Component
public class GenericDAOImpl<T> implements GenericDAO<T> {

	File metadatafile;
	File userfile;
	FileWriter fw;

	List<T> users = new ArrayList<T>();
	private Gson gson;
	private BufferedReader br;


	@Autowired
	MyJson<T> List;

	@SuppressWarnings("unchecked")
	public GenericDAOImpl() throws IOException {
		this.metadatafile = new File("metadata.json");
		this.userfile = new File("users.json");
		this.List = new MyJson<T>();
		this.gson = new Gson();
		if (!metadatafile.exists()) {
			metadatafile.createNewFile();
			this.List.setList((java.util.List<T>) Arrays.asList(new OrgModel("LIC",
					"Lorem ipsum dolor sit amet consectetur adipiscing elit Mauris sagittis pellentesque lacus eleifend lacinia",
					"/static/assets/images/lic.png")));
			doWrite(metadatafile);
		} else {
			gettingListsAndUpdate("metadata.json");
		}
		if (!userfile.exists()) {
			userfile.createNewFile();
			this.List.setList((java.util.List<T>) Arrays.asList(new User("admin", "admin", "ADMIN")));
			doWrite(userfile);
		} else {
			gettingListsAndUpdate("users.json");
		}

	}

	public void doWrite(File file) {
		try {
			this.fw = new FileWriter(file);
			fw.write(gson.toJson(this.List));
			fw.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void gettingListsAndUpdate(String name) {

		try {
			this.br = new BufferedReader(new FileReader(name));

			if (br != null) {

				GsonBuilder gson = new GsonBuilder();
				Type collectionType = new TypeToken<MyJson<T>>() {}.getType();
				this.List = gson.create().fromJson(this.br, collectionType);
			}

			br.close();

		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	@Override
	public List<T> getAllData(String fileName) {
		gettingListsAndUpdate(fileName);
		return this.List.getList();
	}
	
	
}
