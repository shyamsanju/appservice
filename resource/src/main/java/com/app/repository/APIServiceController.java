package com.app.repository;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.app.model.Bussiness;
import com.app.model.MyJson;
import com.app.model.OrgModel;
import com.app.model.User;
import com.app.model.Users;
import com.app.service.BusinessService;
import com.app.service.GenericDAO;
import com.app.service.GenericService;
import com.app.service.UserService;
@Component
@RestController
@RequestMapping("/APIRest")
public class APIServiceController {

	@Autowired
	GenericService<OrgModel> dataservice;
	
/*	@Autowired
	UserService userServices;*/
	/*
	@Autowired
	BusinessService busService;*/
	
	@Autowired
	GenericService<User> userService;
	
	@RequestMapping("/getOrgData")
	public List<OrgModel> getOrgData(){
		
		List<OrgModel> data  = dataservice.getAllOrgData();
		return data;
	}
	
	@RequestMapping("/getAllUsers")
	public List<User> getUsersData(){
		
		List<User> data  =  userService.getAllUsers();
		return data;
		
	}
	
/*	@RequestMapping("/addNew")
	public String addUsers(){
		Users user = new Users("Shyam", "w@w.com", "sss@123");
		Bussiness bus = new Bussiness(12,"SSS");
		busService.saveBusiness(bus);
		//userServices.saveUsers(user);
		return "true";
		
		
	}*/
	
	
}
