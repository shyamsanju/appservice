/**
 * 
 */
package com.app.model;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

/**
 * @author shysatya
 *
 */
@Entity
@Table(name="BUSSINESS")
public class Bussiness {
	
	public Bussiness(int i, String bUSSINESS_NAME) {
		super();
	
		BUSSINESS_NAME = bUSSINESS_NAME;
		BP_ID = i;
	}


	@Id
	@GeneratedValue
	private int BP_ID;
	
	public int getBP_ID() {
		return BP_ID;
	}

	public void setBP_ID(int bP_ID) {
		BP_ID = bP_ID;
	}

	public String getBUSSINESS_NAME() {
		return BUSSINESS_NAME;
	}

	public void setBUSSINESS_NAME(String bUSSINESS_NAME) {
		BUSSINESS_NAME = bUSSINESS_NAME;
	}



	private String BUSSINESS_NAME;
	

	

}
