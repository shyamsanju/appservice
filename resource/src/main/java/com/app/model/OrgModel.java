package com.app.model;

import org.springframework.stereotype.Component;

@Component
public class OrgModel {

	private String title;
	private String Desc;
	private String imgUrl;
	
	public OrgModel(){
		
		
	}
	public OrgModel(String title,String Desc,String imgUrl){
		this.title=title;
		this.Desc=Desc;
		this.imgUrl =imgUrl;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getDesc() {
		return Desc;
	}
	public void setDesc(String desc) {
		Desc = desc;
	}
	public String getImgUrl() {
		return imgUrl;
	}
	public void setImgUrl(String imgUrl) {
		this.imgUrl = imgUrl;
	}
	
}
