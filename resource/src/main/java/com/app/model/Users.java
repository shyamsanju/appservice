/**
 * 
 */
package com.app.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.TableGenerator;

/**
 * @author shysatya
 *
 */
@Entity
@Table(name = "USERS")
public class Users {

	
	@Id
	@GeneratedValue
	private int USER_ID;
	
	private String USER_NAME;
	public Users(String uSER_NAME, String eMAIL, String pASSWORD) {
		super();
		USER_NAME = uSER_NAME;
		EMAIL = eMAIL;
		PASSWORD = pASSWORD;
	}

	private String EMAIL;
	private String PASSWORD;
	
	@ManyToOne(optional = false)
	@JoinColumn(name = "BP_ID")
	private Bussiness bussiness;
	
	public int getUSER_ID() {
		return USER_ID;
	}

	public void setUSER_ID(int uSER_ID) {
		USER_ID = uSER_ID;
	}

	public String getUSER_NAME() {
		return USER_NAME;
	}

	public void setUSER_NAME(String uSER_NAME) {
		USER_NAME = uSER_NAME;
	}

	public String getEMAIL() {
		return EMAIL;
	}

	public void setEMAIL(String eMAIL) {
		EMAIL = eMAIL;
	}

	public String getPASSWORD() {
		return PASSWORD;
	}

	public void setPASSWORD(String pASSWORD) {
		PASSWORD = pASSWORD;
	}

	public Bussiness getBussiness() {
		return bussiness;
	}

	public void setBussiness(Bussiness bussiness) {
		this.bussiness = bussiness;
	}

	
	
	
}
