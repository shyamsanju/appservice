package com.app;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableResourceServer;
import org.springframework.transaction.annotation.EnableTransactionManagement;

@SpringBootApplication
@ComponentScan(basePackages={"com.app"})
@EnableResourceServer
@EnableTransactionManagement
public class ResourceApplication {
	
	
	
	public static void main(String[] args) {
		SpringApplication.run(ResourceApplication.class, args);

	}
	
}
