/*import java.util.ArrayList;
import java.util.Arrays;
import java.util.Set;
import java.util.TreeSet;
import java.util.Comparator;
import java.util.List;

public class RemoveDuplicate12 {

	public static void main(String[] args) {
		Set<Student> set = new TreeSet<Student>();
		List<Student> students = Arrays.asList(new Student("Student1", "1005"), new Student("Student2", "1004"),
				new Student("Student3", "1003"), new Student("Student6", "1002"), new Student("Student5", "1001"),
				new Student("Student6", "1000"));

		// Sorting Using Lambda

		students.sort(new Comparator<Student>() {

			@Override
			public int compare(Student s1, Student s2) {

				return s1.getId().compareTo(s2.getId());
			}

		});

		System.out.println(students);
		set.addAll(students);

		System.out.println("\n***** After removing duplicates *******\n");

		final ArrayList<Student> newList = new ArrayList<Student>(set);

		*//** Printing original list **//*
		System.out.println(newList);
	}

}

class Students implements Comparable<Student> {
	private String name;
	private String id;

	public Student(String name, String id) {
		this.name = name;
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	@Override
	public String toString() {
		return "\n" + "Name=" + name + "   Id=" + id;
	}

	@Override
	public int compareTo(Student o1) {
		if (o1.getName().equalsIgnoreCase(this.name)) {
			return 0;
		}
		return 1;
	}

	// public static Comparator<Student> StudentIdComparator = (Student
	// s1,Student s2) -> s1.getId().compareTo(s2.getId());
}
*/