package com.app;

import java.util.Comparator;

 class Student implements Comparable<Student> {
	private String name;
	private String id;

	public Student(String name, String id) {
		this.name = name;
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	@Override
	public String toString() {
		return "\n" + "Name=" + name + "   Id=" + id;
	}

	@Override
	public int compareTo(Student o1) {
		if (o1.getName().equalsIgnoreCase(this.name)) {
			return 0;
		}
		return 1;
	}

	public static Comparator<Student> StudentIdComparator = new Comparator<Student>() {

		public int compare(Student student1, Student student2) {

			String stdName1 = student1.getId();
			String stdName2 = student2.getId();

			// ascending order
			return stdName1.compareTo(stdName2);

			// descending order
			// return stdName1.compareTo(stdName2);
		}
		public boolean equals(Student student1, Student student2){
			String stdName1 = student1.getId();
			String stdName2 = student2.getId();

		
			return stdName1.equals(stdName2);
			
		}
	};
}
