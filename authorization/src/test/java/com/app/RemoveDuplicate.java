package com.app;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Set;
import java.util.TreeSet;

public class RemoveDuplicate {

	public static void main(String[] args) {

		final ArrayList<Student> students = new ArrayList<Student>();

		Set<Student> set = new TreeSet<Student>();
		Student[] starr = new Student[6];
		starr[0] = new Student("Student1", "1005");
		starr[1] = new Student("Student2", "1004");
		starr[2] = new Student("Student3", "1003");
		starr[3] = new Student("Student6", "1002");
		starr[4] = new Student("Student5", "1001");
		starr[5] = new Student("Student6", "1000");

		Arrays.sort(starr, Student.StudentIdComparator);
		for (Student s : starr) {
			students.add(s);
		}

		System.out.println(students);
		set.addAll(students);

		System.out.println("\n***** After removing duplicates *******\n");

		final ArrayList<Student> newList = new ArrayList<Student>(set);

		/** Printing original list **/
		System.out.println(newList);
	}

}