package com.app;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api")
public class RestControllerService {

	@Autowired
	Userservice userService;
	
    @RequestMapping(value = "/addUser", method = RequestMethod.POST)
    public ResponseEntity<String> addUser(@RequestBody User user) {
    	user.setRole("USER");
    	if(userService.findUser(user)!=null){
    		return new ResponseEntity<String>("User name already exists",HttpStatus.OK);
    	}
       String resp  = userService.addUser(user);
        if(resp == null){
            return new ResponseEntity<String>(HttpStatus.EXPECTATION_FAILED);//You many decide to return HttpStatus.NOT_FOUND
        }
        return new ResponseEntity<String>(resp, HttpStatus.OK);
    }
    	
}
